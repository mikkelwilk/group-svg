import xml.etree.ElementTree as ET
import os

ET.register_namespace('', 'http://www.w3.org/2000/svg')
root = ET.Element("svg")

for file in os.listdir('SVG'):
    #import file tree of letter
    LetterTree = ET.parse('SVG/' + file)
    LetterRoot = LetterTree.getroot()
    #create wrapper for paths
    LetterWrapper = ET.SubElement(root, "g")
    #add width and heighth of the letter
    LetterWrapper.attrib['height'] = LetterRoot.attrib['height']
    LetterWrapper.attrib['width'] = LetterRoot.attrib['width']
    #set letter value
    LetterWrapper.attrib['letter'] = os.path.splitext(file)[0]
    #add paths to wrapper
    for path in LetterTree.findall('.//{http://www.w3.org/2000/svg}path'):
        LetterWrapper.append(path)

ET.ElementTree(root).write('test.svg')