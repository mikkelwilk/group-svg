import xml.etree.ElementTree as ET

#namespace
ET.register_namespace('', 'http://www.w3.org/2000/svg')

sentence = list("a robot wrote this sentence")
FontDir = "mikkel.svg"

#create root element
root = ET.Element("svg")

#define default spacing values
WidthPos = 0
HeightPos = 0
heighest = 0


#parse letters
for letter in sentence:
    #check if space letter
    if letter == " ":
        letter = "space"
    #import font file
    FontTree = ET.parse(FontDir)
    #find letter in font file
    LetterPaths = FontTree.find('.//{http://www.w3.org/2000/svg}g[@letter="%s"]' % letter)
    #append letter to main root
    root.append(LetterPaths)
    #find heighest letter
    if LetterPaths.attrib['height'] > heighest:
        heighest = LetterPaths.attrib['height']

#space letters
for letter in root.findall('.//{http://www.w3.org/2000/svg}g'):
    #calculate drop of letter
    LetterDrop = float(heighest) - float(letter.attrib['height'])
    #add extra drop if exists
    if 'drop' in letter.attrib:
        LetterDrop += float(letter.attrib['drop'])

    #add matrix to letter
    letter.attrib['transform'] = 'matrix(1,0,0,1,%s,%s)' % (WidthPos, LetterDrop)
    #increment widthpos
    WidthPos += float(letter.attrib['width'])+5
    

ET.ElementTree(root).write('output.svg')
